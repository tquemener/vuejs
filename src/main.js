import Vue from 'vue' //librairie "vue" dans node_modules
import VueRouter from 'vue-router'
import app from './app.vue' //fichier app.vue local
import MovieItemComponent from './components/movieitem.vue'
import home from './pages/home.vue'
import newMovie from './pages/new.vue'
import editMovie from './pages/edit.vue'
import infoMovie from './pages/movie.vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'

Vue.component('movie-item', MovieItemComponent);
Vue.use(VueRouter);
Vue.use(Vuetify)

window.shared_data = ({
    movies : [
      { 
          id:0,
          title: "Django Unchained",
          lang:"Anglais",
          img:"https://cdn-s-www.republicain-lorrain.fr/images/36776e02-4297-454f-a6da-0a27f3e1144c/BES_06/illustration-django-unchained_1-1531087962.jpg",
          year: "2012",
          type:"Action",
          synopsys: "Dans le sud des États-Unis, deux ans avant la guerre de Sécession, le Dr King Schultz, un chasseur de primes allemand, fait l’acquisition de Django, un esclave qui peut l’aider à traquer les frères Brittle, les meurtriers qu’il recherche.",
          dirFirstname:"Quentin",
          dirLastname:"Tarantino",
          dirNation:"Americain",
          dirBorn:"27-03-1963",
          note:4
      },
      { 
          id:1,
          title: "Deadpool",
          lang:"Anglais",
          img:"https://m.media-amazon.com/images/M/MV5BYzE5MjY1ZDgtMTkyNC00MTMyLThhMjAtZGI5OTE1NzFlZGJjXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_SY1000_CR0,0,666,1000_AL_.jpg",
          year: "2016",
          type:"Action",
          synopsys: "Deadpool, est l'anti-héros le plus atypique de l'univers Marvel. A l'origine, il s'appelle Wade Wilson : un ancien militaire des Forces Spéciales devenu mercenaire. Après avoir subi une expérimentation hors norme qui va accélérer ses pouvoirs de guérison, il va devenir Deadpool.",
          dirFirstname:"Tim",
          dirLastname:"Miller",
          dirNation:"Americain",
          dirBorn:"28-02-70",
          note:3
      }
  ]
})

const routes = [
    { path: '/', component: home, name:"home" },
    { path: '/movie/:id', component: infoMovie, name:"infoMovie" },
    { path: '/movie/new', component: newMovie, name:"newMovie" },
    { path: '/movie/edit/:id', component: editMovie, name:"editMovie" }
]

const router = new VueRouter({
  routes
})


new Vue({
  el: '#app',
  render: h => h(app),
  router
})
